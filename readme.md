 # Bonjour, bienvenue sur le gitlab du tp6 de conception logiciel

# Getting Started with serveur web

if you can read this sentence, you have succesfully cloned the git deposit. To launch one app, you have to follow these steps:
    
    - go to the root of the folder tp5-logicielle/serveurWeb

    -write in the shell pip install -r requirements.txt     

    -then write in the shell uvicorn main:app --reload     




# Getting Started with client

if you can read this sentence, you have succesfully cloned the git deposit. To launch one app, you have to follow these steps:
    
    - go to the root of the folder tp5-logicielle/client

    -write in the shell pip install -r requirements.txt     

    -then write in the shell uvicorn python main.py    
    
    


